# Docker Image for SSH and rsync

**Image Location**: `registry.gitlab.com/sebastians90/docker-alpine-ssh-rsync` (built by [GiltLab CI](https://gitlab.com/SebastianS90/docker-alpine-ssh-rsync/pipelines))

This Docker image is based on [alpine](https://hub.docker.com/_/alpine/) and comes with the following packages installed:

- `openssh-client`
- `rsync`

## Contributing
The canonical source of this Docker image is [hosted on GitLab.com](https://gitlab.com/SebastianS90/docker-alpine-ssh-rsync).
Please file any issues [there](https://gitlab.com/SebastianS90/docker-alpine-ssh-rsync/issues).

## License
This Dockerfile and the CI configuration is released into the public domain. Please see the [LICENSE](LICENSE) file for details.
For the license of software that is downloaded by this Dockerfile, please refer to their appropriate vendors.
