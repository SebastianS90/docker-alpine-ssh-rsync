FROM alpine:latest
MAINTAINER Sebastian Schweizer <sebastian@schweizer.tel>

RUN apk add --no-cache openssh-client rsync
